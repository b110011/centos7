#!/usr/bin/env bash

# If not running interactively, don't do anything
if [[ -z "$PS1" ]]; then
	return
fi

#-----------------------------------------------------------------------------#
# Settings                                                                    #
#-----------------------------------------------------------------------------#

# Bash (additional shell optional behavior)

shopt -s autocd
short -s cdable_vars
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s direxpand
shopt -s dirspell
shopt -s dotglob
shopt -s expand_aliases
shopt -s extquote
shopt -s globstar
shopt -s gnu_errfmt
shopt -s histappend
shopt -s nocaseglob
shopt -s progcomp

# Bash (history)

export HISTCONTROL=ignoredups

export HISTIGNORE="&:[bf]g:clear:exit:history *:htop:la;ll:ls;pwd:top"

export HISTFILE="${XDG_DATA_HOME}/bash/history"

export HISTTIMEFORMAT="[%Y-%m-%d %H:%M:%S]: "

export HISTSIZE=16384

export HISTFILESIZE="${HISTSIZE}"

# Default browser

if [[ -n "${DISPLAY}" ]]; then
    export BROWSER=firefox
    #export BROWSER=google-chrome
fi

# Default editor

if [[ -n "${DISPLAY}" ]]; then
    export EDITOR="nano"
    #export EDITOR="vim"
else 
    export EDITOR="geany -imnst"
    #export EDITOR="subl -n -w"
fi

# GDB (GNU Debugger)

alias gdb="gdb -nh -x ${XDG_CONFIG_HOME}/gdb/init "

# GNUPG

export GNUPGHOME="${XDG_CONFIG_HOME}/gnupg"

# ICEauthority

export ICEAUTHORITY="${XDG_CACHE_HOME}/ICEauthority"

# Konsole and Gnome Terminal

export GROFF_NO_SGR=1

# Less

export LESS="-R"

export LESSHISTFILE="${XDG_DATA_HOME}/less/history"
#export LESSKEY="${XDG_CONFIG_HOME}/less/lesskey"

export LESS_TERMCAP_mb=$'\e[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\e[1;33m'     # begin blink
export LESS_TERMCAP_so=$'\e[01;44;37m' # begin reverse video
export LESS_TERMCAP_us=$'\e[01;37m'    # begin underline
export LESS_TERMCAP_me=$'\e[0m'        # reset bold/blink
export LESS_TERMCAP_se=$'\e[0m'        # reset reverse video
export LESS_TERMCAP_ue=$'\e[0m'        # reset underline

# Pager

export MANPAGER='less -s -M +Gg'
export PAGER="less"

# Python

export PYTHONIOENCODING='UTF-8'

# Tmux

export TMUX_TMPDIR="${XDG_RUNTIME_DIR}"

alias tmux="tmux -f ${XDG_CONFIG_HOME}/tmux/tmux.conf "

# readline

export INPUTRC="${XDG_CONFIG_HOME}/readline/inputrc"

# rxvt-unicode

export RXVT_SOCKET="${XDG_RUNTIME_DIR}/urxvtd"

# Vim

export VIMINIT='let $MYVIMRC="${XDG_CONFIG_HOME}/vim/vimrc" | source ${MYVIMRC}'
export VIMDOTDIR="${XDG_CONFIG_HOME}/vim"

# Subversion

export SUBVERSION_HOME="${XDG_CONFIG_HOME}/subversion"

# X Window System / Xorg / X11

export ERRFILE="${XDG_DATA_HOME}/X11/xsession-errors"

export XAUTHORITY="${XDG_CACHE_HOME}/X11/xauthority"
export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"
export XSERVERRC="${XDG_CONFIG_HOME}/X11/xserverrc"

#-----------------------------------------------------------------------------#
# Includes                                                                    #
#-----------------------------------------------------------------------------#

# Enable programmable completion features (you don't need to enable this, if it's
# already enabled in /etc/bash.bashrc and /etc/profile sources /etc/bash.bashrc).
#if [[ -f /etc/bash_completion ]]; then
#    source /etc/bash_completion
#fi

if [[ -f "${XDG_CONFIG_HOME}/dircolor/dircolorrc" ]]; then
	eval `dircolors "${XDG_CONFIG_HOME}/dircolor/dircolorrc"`
fi

if [[ -f "${HOME}/.local/bin/git-svn-diff.sh" ]]; then
    source "${HOME}/.local/bin/git-svn-diff.sh"
fi

if [[ -f "${HOME}/.local/bin/git-svn-prompt.sh" ]]; then
    source "${HOME}/.local/bin/git-svn-diff.sh"
fi

if [[ -f "${HOME}/.local/bin/svn-color-output.sh" ]]; then
    source "${HOME}/.local/bin/svn-color-output.sh"
fi

#-----------------------------------------------------------------------------#
# Software Collections (SCL)                                                  #
#-----------------------------------------------------------------------------#

# Enable binutils
source scl_source enable sclo-devtoolset-8-binutils

# Enable elfutils
source scl_source enable sclo-devtoolset-8-elfutils

# Enable GDB (GNU Debugger)
source scl_source enable sclo-devtoolset-8-gdb

# Enable Git
source scl_source enable sclo-git25

# Enable Valgrind
source scl_source enable sclo-devtoolset-8-valgrind

#-----------------------------------------------------------------------------#
# Aliases                                                                     #
#-----------------------------------------------------------------------------#

alias aliases='cat ~/.bashrc | grep alias'

alias callgrind='valgrind --tool=callgrind'

alias count='wc -l'

alias del='gvfs-trash'

alias du="du -h"
alias df="df -h"

alias grep='grep -n --color=always'

alias ls='ls -h --color=always'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

alias reload="exec ${SHELL} -l"

alias su='sudo -s'

#-----------------------------------------------------------------------------#
# Functions                                                                   #
#-----------------------------------------------------------------------------#

